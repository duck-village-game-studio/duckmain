# DuckMain

## [API](https://gitlab.com/duck-village-game-studio/duckmain/wikis/Api)


### Start development
```bash
docker-compose up --build
```
### Start
```bash
go run src/main.go
```
### Build
```bash
go build -o bin/main src/main.go
```
### How to debug/develop
Recommended way is to:
1) Start docker-compose
```bash
docker-compose up --build
```
2) Open VSCode and ssh into container
3) .vscode lanuch.json
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Launch file",
            "type": "go",
            "request": "launch",
            "mode": "debug",
            "program": "/app/src/main.go",
            "env": {
                "PORT": 3002
            }
        }
    ]
}
```
4) Open Debug tab. Now you should be able to set breakpoints.
In order to request server with attached debugger, 
```bash
curl 0.0.0.0:3002
```