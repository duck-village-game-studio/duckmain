FROM golang:1.13.5-buster AS builder
COPY . /app
WORKDIR /app
ENV PORT=8080
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bin/main src/main.go

FROM scratch
COPY --from=builder app/bin/main /go/bin/main
ENTRYPOINT [ "/go/bin/main" ]