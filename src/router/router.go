package router

import (
	"fmt"
	"net/http"

	handler "duckmain.com/main/src/router/handler"
	"github.com/gorilla/mux"
)

func enableCors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		next.ServeHTTP(w, req)
	})
}

type MiddlewareFunc func(http.Handler) http.Handler

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		fmt.Println(req.Method, req.URL)
		next.ServeHTTP(w, req)
	})
}

//SetupRouter whatever
func SetupRouter() {
	router := mux.NewRouter()
	router.Use(mux.CORSMethodMiddleware(router))
	router.Use(loggingMiddleware)
	router.Use(enableCors)

	router.HandleFunc("/", handler.HomepageHandler)
	router.HandleFunc("/status", handler.StatusHandler)
	router.HandleFunc("/login", handler.LoginHandler)
	http.Handle("/", router)
}
