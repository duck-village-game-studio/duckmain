package handler

import (
	"encoding/json"
	"net/http"
)

type authTokenResponse struct {
	Token string `json:"token"`
}

// LoginHandler Handle login
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	response := authTokenResponse{}
	response.Token = "mocked-token"
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		panic(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jsonResponse)
}
