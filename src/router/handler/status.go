package handler

import (
	"net/http"
)

// StatusHanlder Handle status
func StatusHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}
