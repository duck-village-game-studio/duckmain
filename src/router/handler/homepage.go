package handler

import (
	"fmt"
	"net/http"
)

// HomepageHandler Handle homepage
func HomepageHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Homepage")
}
