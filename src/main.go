package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	router "duckmain.com/main/src/router"
)

func main() {
	const HOST string = "0.0.0.0"
	var PORT string = os.Getenv("PORT")
	var url string = fmt.Sprintf("%s:%s", HOST, PORT)

	router.SetupRouter()
	log.Println("Starting server on:", url)
	log.Fatal(http.ListenAndServe(url, nil))
}