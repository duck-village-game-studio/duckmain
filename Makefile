GO=go
DOCKER=docker
DOCKER_IMAGE_NAME=registry.gitlab.com/duck-village-game-studio/duckmain
PORT=3001

start:
	${DOCKER} build -t duckmain .
	${DOCKER} run -p 3001:3001 -it duckmain bash

build:
	${DOCKER} build . -t ${DOCKER_IMAGE_NAME}

run:
	${DOCKER} run -p ${PORT}:${PORT} -e PORT=${PORT} ${DOCKER_IMAGE_NAME}

push:
	${DOCKER} push ${DOCKER_IMAGE_NAME}
start-compose:
	@echo "Running docker-compose server"
	docker-compose up --build